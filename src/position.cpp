// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "position_p.h"

#include <QGuiApplication>
#include <QScreen>

#include <QDBusInterface>
#include <QDBusPendingReply>

#include <QQuickWindow>

#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/plasmashell.h>
#include <KWayland/Client/registry.h>
#include <KWayland/Client/surface.h>

static KWayland::Client::Registry* getRegistry()
{
	static QPointer<KWayland::Client::Registry> m_registry = {};
	if (m_registry) {
		return m_registry;
	}

	KWayland::Client::ConnectionThread *connection = KWayland::Client::ConnectionThread::fromApplication(qApp);
	if (!connection) {
		return nullptr;
	}

	m_registry = new KWayland::Client::Registry(qApp);
	m_registry->create(connection);
	m_registry->setup();

	connection->roundtrip();

	return m_registry;
}

static KWayland::Client::PlasmaShell *getPlasmaIface()
{
	auto reg = getRegistry();
	static QPointer<KWayland::Client::PlasmaShell> pshell = {};

	if (!pshell && reg) {
		const KWayland::Client::Registry::AnnouncedInterface interface = reg->interface(KWayland::Client::Registry::Interface::PlasmaShell);

		if (interface.name == 0) {
			return nullptr;
		}

		pshell = reg->createPlasmaShell(interface.name, interface.version, qApp);

		QObject::connect(pshell, &KWayland::Client::PlasmaShell::removed, pshell, []() {
			pshell->deleteLater();
		});
	}

	return pshell;
}

Positioner::Positioner(QObject *parent) : QObject(parent), d(new Private)
{
	static QDBusInterface strutManager("org.kde.plasmashell", "/StrutManager", "org.kde.PlasmaShell.StrutManager");

	d->screen = QGuiApplication::screenAt(QCursor::pos());
	if (!d->screen) {
		d->screen = QGuiApplication::screens().first();
	}

	QDBusPendingCall async = strutManager.asyncCall("availableScreenRect", d->screen->name());
	QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(async, this);
	QObject::connect(watcher, &QDBusPendingCallWatcher::finished, this, [this, watcher]() {
		QDBusPendingReply<QRect> reply = *watcher;

		d->space = reply.isValid() ? reply.value() : QRect();

		reposition();
	});
}

Positioner::~Positioner()
{

}

int Positioner::x() const
{
	return d->x;
}

void Positioner::setX(int x)
{
	if (d->x == x) {
		return;
	}

	d->x = x;

	reposition();
}

auto plasmaSurf(QQuickWindow* w) -> KWayland::Client::PlasmaShellSurface* {
	auto surf = KWayland::Client::Surface::fromWindow(w);
	if (!surf) {
		return nullptr;
	}

	auto psurf = getPlasmaIface()->createSurface(surf);
	if (!psurf) {
		return nullptr;
	}

	return psurf;
}

void Positioner::reposition()
{
	if (!d->window || !d->tabWindow) {
		return;
	}

	auto ww = d->window;
	auto tw = d->tabWindow;
	auto w = plasmaSurf(d->window), t = plasmaSurf(d->tabWindow);

	if (!w || !t) {
		return;
	}

	auto bottom = d->space.bottom() + 1;

	d->ani.setStartValue(bottom);
	d->ani.setEndValue(bottom - ww->height());

	w->setPosition(QPoint(d->screen->geometry().x() + d->x, d->ani.currentValue().toInt()));
	t->setPosition(QPoint(d->screen->geometry().x() + d->x, d->ani.currentValue().toInt() - tw->height() ));
}

Positioner *Positioner::qmlAttachedProperties(QObject *object)
{
	auto pos = new Positioner(object);

	pos->d->window = qobject_cast<QQuickWindow*>(object);
	pos->d->ani.setEasingCurve(QEasingCurve(QEasingCurve::InOutQuad));
	pos->d->ani.setDuration(200);
	connect(&pos->d->ani, &QVariantAnimation::valueChanged, pos, &Positioner::reposition);

	if (pos->d->window == nullptr) {
		qFatal("used a Positioner on a non-window");
	}

	return pos;
}

bool Positioner::expanded()
{
	return d->expanded;
}

void Positioner::setExpanded(bool expanded)
{
	if (d->expanded == expanded) {
		return;
	}
	d->expanded = expanded;

	d->ani.setDirection(expanded ? QVariantAnimation::Forward : QVariantAnimation::Backward);
	d->ani.start();

	reposition();
}

QQuickWindow* Positioner::tabWindow()
{
	return d->tabWindow;
}

void Positioner::setTabWindow(QQuickWindow* tabWindow)
{
	if (d->tabWindow == tabWindow) {
		return;
	}
	d->tabWindow = tabWindow;

	reposition();
}
