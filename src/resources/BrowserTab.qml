// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import QtWebEngine 1.10

import org.kde.browsething 1.0 as B

Window {
	id: root

	width: colLayout.implicitWidth
	height: colLayout.implicitHeight
	visible: true
	flags: Qt.FramelessWindowHint
	color: "transparent"

	signal doClose()

	B.Positioner.x: 50
	B.Positioner.tabWindow: tabWindow
	B.Positioner.expanded: root.active || tabWindow.active

	function reevaluate() {
		B.Positioner.expanded = root.active || tabWindow.active
	}

	onActiveChanged: root.reevaluate()

	// readonly property bool expanded: tabHover.hovered || toolbarHover.hovered || webEngineHover.hovered || theDragHandler.active
	// onExpandedChanged: debouncer.start()

	// Timer {
	// 	id: debouncer

	// 	interval: 100
	// 	onTriggered: root.B.Positioner.expanded = tabHover.hovered || toolbarHover.hovered || webEngineHover.hovered || theDragHandler.active
	// }

	Window {
		id: tabWindow

		width: theTab.implicitWidth
		height: theTab.implicitHeight

		flags: Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint

		visible: true
		color: "transparent"
		onActiveChanged: root.reevaluate()

		QQC2.TabButton {
			id: theTab

			text: waso.title
			checked: true

			DragHandler {
				id: theDragHandler

				target: null
				yAxis.enabled: false
				onTranslationChanged: root.B.Positioner.x += translation.x
			}

			Layout.alignment: Qt.AlignHCenter
		}
	}

	ColumnLayout {
		id: colLayout

		anchors.fill: parent

		spacing: 0

		WebEngineView {
			id: waso

			url: "https://www.qt.io"
			onUrlChanged: urlBar.text = url

			Layout.minimumWidth: 1000
			Layout.minimumHeight: 500

			Layout.fillWidth: true
			Layout.fillHeight: true

		}

		QQC2.ToolBar {
			position: QQC2.ToolBar.Footer

			opacity: theDragHandler.active ? 0.5 : 1.0

			Layout.fillWidth: true

			// Layout.leftMargin: 100
			// Layout.rightMargin: 100

			contentItem: RowLayout {
				component WebActionButton : QQC2.Button {
					required property WebEngineAction webAction

					text: webAction.text
					enabled: webAction.enabled

					onClicked: webAction.trigger()
				}
				WebActionButton {
					webAction: waso.action(WebEngineView.Forward)
				}
				WebActionButton {
					webAction: waso.action(WebEngineView.Back)
				}
				WebActionButton {
					webAction: waso.action(WebEngineView.Reload)
				}
				WebActionButton {
					webAction: waso.action(WebEngineView.Stop)
				}
				QQC2.TextField {
					id: urlBar

					Layout.fillWidth: true

					onAccepted: {
						waso.url = text
					}
				}
				QQC2.Button {
					text: "Close"
					onClicked: root.doClose()
				}
				QQC2.Button {
					text: "New Tab"
					onClicked: application.createTab()
				}
			}
		}
	}
}