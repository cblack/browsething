// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import qbs.Process

QtApplication {
	name: "browsething"

	files: [
		"*.cpp",
		"*.h",
	]

	Group {
		files: ["resources/**"]
		fileTags: "qt.core.resource_data"
		Qt.core.resourceSourceBase: "resources/"
		Qt.core.resourcePrefix: "/"
	}

	Probe {
		id: mu
		property string src: product.sourceDirectory
		property var linkerFlags
		property var includeDirs
		configure: {
			var proc = new Process()
			var exitCode = proc.exec("bash", [mu.src + "/extract_flags.sh",
				"find_package(KF5Wayland REQUIRED)\n",

				"KF5::WaylandClient",
			])
			if (exitCode != 0) {
				console.error(proc.readStdOut())
				throw "extracting flags from CMake libraries failed"
			}
			var stdout = proc.readStdOut()
			stdout = stdout.split("====")
			linkerFlags = stdout[0].split("\n").filter(function(it) { return Boolean(it) && !it.contains("rpath") && (it.startsWith("/") || it.startsWith("-l")) }).map(function(it) { return it.replace("-Wl,", "") })
			includeDirs = stdout[1].split("\n").filter(function(it) { return Boolean(it) && !it.contains("rpath") && (it.startsWith("/") || it.startsWith("-l")) }).map(function(it) { return it.replace("-Wl,", "") })
		}
	}
	cpp.driverLinkerFlags: mu.linkerFlags
	cpp.includePaths: mu.includeDirs
	cpp.cxxLanguageVersion: "c++20"

	Depends { name: "Qt"; submodules: ["dbus", "widgets", "qml", "quick", "quickcontrols2", "webengine"] }
}
