// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QObject>
#include <QtQml>

class QQuickWindow;

class Positioner : public QObject
{
	Q_OBJECT

	Q_PROPERTY(int x READ x WRITE setX NOTIFY xChanged)
	Q_PROPERTY(QQuickWindow* tabWindow READ tabWindow WRITE setTabWindow NOTIFY tabWindowChanged)
	Q_PROPERTY(bool expanded READ expanded WRITE setExpanded NOTIFY expandedChanged)

public:
	explicit Positioner(QObject *parent = nullptr);
	~Positioner() override;

	int x() const;
	void setX(int x);
	Q_SIGNAL void xChanged();

	bool expanded();
	void setExpanded(bool expanded);
	Q_SIGNAL void expandedChanged();

	QQuickWindow* tabWindow();
	void setTabWindow(QQuickWindow* tabWindow);
	Q_SIGNAL void tabWindowChanged();

	Q_INVOKABLE void reposition();

	static Positioner *qmlAttachedProperties(QObject *object);

private:
	struct Private;
	QScopedPointer<Private> d;

};

QML_DECLARE_TYPEINFO(Positioner, QML_HAS_ATTACHED_PROPERTIES)

