// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <QApplication>
#include <QtWebEngine>

#include "setup.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("KDE");

	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

	QtWebEngine::initialize();

	QApplication app(argc, argv);

	setup();

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));

	return app.exec();
}
