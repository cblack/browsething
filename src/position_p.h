// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "position.h"

class QQuickWindow;
class QScreen;

struct Positioner::Private
{
	int x = 0;
	bool expanded = false;
	QRect space = QRect();
	QQuickWindow* window = nullptr;
	QQuickWindow* tabWindow = nullptr;
	QScreen* screen = nullptr;

	QVariantAnimation ani;
};
