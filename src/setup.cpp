// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "position.h"

#include "setup.h"

void setup() {
	qRegisterMetaType<Positioner*>();
	qmlRegisterUncreatableType<Positioner>("org.kde.browsething", 1, 0, "Positioner", "is attached only");
}