// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.10

QtObject {
	id: application

	property Component tabs: BrowserTab {

	}

	function createTab() {
		let obj = application.tabs.createObject(application)
		obj.doClose.connect(() => {
			obj.visible = false
			obj.destroy()
		})
	}

	Component.onCompleted: createTab()
}